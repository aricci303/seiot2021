#include "ExpManTask.h"
#include "Arduino.h"
#include "Logger.h"
#include "config.h"
#include "BlinkingTask.h"

ExpManTask::ExpManTask(Experiment* pExp, UserConsole* pConsole, BlinkingTask* pBlink): pExperiment(pExp), pUserConsole(pConsole), pBlinkingTask(pBlink) {
  Logger.log(String("EXPMAN | => ready "));      
  pUserConsole->reset();
  pUserConsole->notifyReady();
  state = READY;
  sleepTime = -1;
}
  
void ExpManTask::tick(){
  switch (state){
    case IDLE: {
      if (pUserConsole->detectedPresence()){
        Logger.log(String("EXPMAN | => ready "));      
        pUserConsole->reset();
        pUserConsole->notifyReady();
        state = READY;
        sleepTime = -1;
      }
      break;
    }
    case READY: {
      if (pUserConsole->started()){
        TTrackFreq freq = pUserConsole->getSelectedFreq();
        pExperiment->setTrackingFreq(freq);
        pExperiment->init();
        Logger.log(String("EXPMAN | => ongoing at freq ") + freq);
        state = ONGOING;
        pUserConsole->notifyStarted(freq);
      } else if (!pUserConsole->detectedPresence()){
        if (sleepTime == -1){
          sleepTime = millis();
        } else {
          if (millis() - sleepTime > SLEEP_TIME){
            Logger.log(String("EXPMAN | => idle "));
            pUserConsole->notifySleeping();
            state = IDLE;
            Logger.log(String("EXPMAN | going to sleep... "));
            pUserConsole->sleep();
            Logger.log(String("EXPMAN | awaken. "));
          }
        }        
      }
      break;
    }
    case ONGOING: {
      long time = pExperiment->getLastTime();
      if (pExperiment->isObjLost()){
        pExperiment->setInterrupted();
        Logger.log(String("EXPMAN | => error "));
        pUserConsole->notifyError();
        pBlinkingTask->setActive(true);
        errorTime = millis();
        state = ERROR;        
      } else if (pUserConsole->stopped() || time > MAX_TIME){
          Logger.log(String("EXPMAN | => completed "));
          pExperiment->setCompleted();
          pUserConsole->notifyCompleted();
          state = COMPLETED;
      }
      break;
    }
    case COMPLETED: {
      if (pUserConsole->isUserOK()){
        Logger.log(String("EXPMAN | => user OK "));      
        pUserConsole->reset();
        pUserConsole->notifyReady();
        sleepTime = -1;
        state = READY;  
      }
      break;
    }
    case ERROR: {
      if (millis() - errorTime > ERROR_TIME){
        pBlinkingTask->setActive(false);
        Logger.log(String("EXPMAN | => ready "));      
        pUserConsole->reset();
        pUserConsole->notifyReady();
        sleepTime = -1;
        state = READY;
      }
      break;
    }
  }
};
