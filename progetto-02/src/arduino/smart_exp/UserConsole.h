#ifndef __USER_CONSOLE__
#define __USER_CONSOLE__

#include "config.h"
#include "Pot.h"
#include "Pir.h"
#include "Button.h"
#include "light.h"
#include "Experiment.h"

class UserConsole {

public:
  UserConsole();

  void reset();
  void sleep();
  
  bool started();
  bool stopped();
  bool detectedPresence();

  TTrackFreq getSelectedFreq();
  
  Light* getLed1();
  Light* getLed2();

  void notifyCalibrating();
  void notifySleeping();
  void notifyReady();
  void notifyStarted(TTrackFreq freq);
  void notifyNewData(long time, double pos, double vel, double acc);
  void notifyCompleted();
  void notifyError();

  bool isUserOK();  
  void calibrate();

private:
  Light* pLed1;
  Light* pLed2;
  Potentiometer* pPot;
  Pir* pPir;
  Button* pButtonStart; 
  Button* pButtonStop; 

};

#endif
