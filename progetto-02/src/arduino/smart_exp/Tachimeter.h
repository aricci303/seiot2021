#ifndef __TACHIMETER__
#define __TACHIMETER__

#include "servo_motor.h"

class Tachimeter {

public: 
  Tachimeter(double maxSpeed);
  void enable();
  void disable();
  void setSpeed(double speed);

private:
  ServoMotor* pServo; 
  double maxSpeed; 
};


#endif
