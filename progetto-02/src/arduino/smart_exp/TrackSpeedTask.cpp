#include "TrackSpeedTask.h"
#include "Arduino.h"
#include "Logger.h"
#include "config.h"


TrackSpeedTask::TrackSpeedTask(Experiment* pExp) : pExperiment(pExp) {
  pTachimeter = new Tachimeter(0.5);
  state = IDLE;
};

  
void TrackSpeedTask::tick(){
  switch (state){
    case IDLE: {
      if (pExperiment->started()){
        // Logger.log(String("SINGLE | ready ") + pRadar->getScanTime());
        // pUserConsole->notifyMode(USER_CONSOLE_MODE_SINGLE);
        // pUserConsole->reset();
        // state = WAIT_FOR_PRESENCE;
       pTachimeter->enable();
       Logger.log(String("TRACKSPEEDTASK | => working "));
       state = WORKING;
      }
      break;
    }
    case WORKING: {
      if (!pExperiment->interrupted() && !pExperiment->completed()){
        double speed = abs(pExperiment->getLastVel()*1000);
        Logger.log(String("TRACKSPEEDTASK | ") + speed);
        pTachimeter->setSpeed(speed);
      } else {
        pTachimeter->disable();
        state = IDLE;
      }
      break;
    }
  }
};
