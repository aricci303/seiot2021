#include "UserConsole.h"
#include "Arduino.h"
#include "Logger.h"
#include "ButtonImpl.h"
#include "Led.h"
#include "MsgService.h"
#include <avr/sleep.h>

// used to avoid loosing decimals 
#define PRECISION_MULTIPLIER 1000000

void wakeUp(){}

UserConsole::UserConsole(){
  pPot = new Potentiometer(POT_PIN);
  pPir = new Pir(PIR_PIN);
  pButtonStart = new ButtonImpl(BT_START_PIN);
  pButtonStop = new ButtonImpl(BT_STOP_PIN);
  pLed1 = new Led(L1_PIN);
  pLed2 = new Led(L2_PIN);
  attachInterrupt(digitalPinToInterrupt(PIR_PIN), wakeUp, RISING); 
}

void UserConsole::calibrate(){
  pPir->calibrate();
}


void UserConsole::sleep(){
    pLed1->switchOff();
    pLed2->switchOff();
    delay(100);
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);  
    sleep_enable();
    sleep_mode();  
    reset();
}

void UserConsole::reset(){
  pLed1->switchOn();
  pLed2->switchOff();
};


bool UserConsole::started(){
  return pButtonStart->isPressed();
}

bool UserConsole::stopped(){
  return pButtonStop->isPressed();
}

bool UserConsole::detectedPresence(){
  return pPir->isDetected();
}

Light* UserConsole::getLed1(){
  return pLed1;
}

Light* UserConsole::getLed2(){
  return pLed2;
}

void UserConsole::notifySleeping(){
  MsgService.sendMsg("ex:zz");
}

void UserConsole::notifyReady(){
  MsgService.sendMsg("ex:ok");
}

void UserConsole::notifyCalibrating(){
  MsgService.sendMsg("ex:ca");
}

void UserConsole::notifyStarted(TTrackFreq freq){
  MsgService.sendMsg(String("ex:on:")+freq);
}

void UserConsole::notifyError(){
  MsgService.sendMsg("ex:er");
}

void UserConsole::notifyCompleted(){
  MsgService.sendMsg("ex:co");
}

void UserConsole::notifyNewData(long time, double pos, double vel, double acc){
  MsgService.sendMsg(String("ex:nd:")+time+":"+(pos*PRECISION_MULTIPLIER)+":"+(vel*PRECISION_MULTIPLIER)+":"+(acc*PRECISION_MULTIPLIER));
}

TTrackFreq UserConsole::getSelectedFreq(){
  float value = pPot->getValue();  
  if (value < 0.25){
    return FREQ_1HZ;
  } else if (value < 0.5){
    return FREQ_5HZ;    
  } else if (value < 0.75){
    return FREQ_25HZ;    
  } else {
    return FREQ_50HZ;    
  }
}
  
bool UserConsole::isUserOK(){
  if (MsgService.isMsgAvailable()){
    Msg* msg = MsgService.receiveMsg();
    if (msg != NULL){
      delete msg;
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}
