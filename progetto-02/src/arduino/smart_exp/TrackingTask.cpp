#include "TrackingTask.h"
#include "Arduino.h"
#include "Logger.h"
#include "config.h"
#include "TempSensorLM35.h"

TrackingTask::TrackingTask(Experiment* pExp, UserConsole* pConsole) : pExperiment(pExp), pUserConsole(pConsole) {
  pSonar = new Sonar(SONAR_ECHO_PIN, SONAR_TRIG_PIN,10000);
  pTemp = new TempSensorLM35(TEMP_PIN);
  state = IDLE;
  dt = 40;
};

void TrackingTask::init(){
  Task::init(BASE_PERIOD);
  state = IDLE;
};

  
void TrackingTask::tick(){
  switch (state){
    case IDLE: {
      if (pExperiment->started()){
        TTrackFreq freq = pExperiment->getTrackingFreq();
        switch (freq){
          case FREQ_1HZ:
            dt = 1000;
            break;
          case FREQ_5HZ:
            dt = 200;
            break;
          case FREQ_25HZ:
            dt = 40;
            break;
          case FREQ_50HZ:
            dt = 20;
            break;
        }
       // Logger.log(String("TRACKTASK | => working "));
       float temp = pTemp->getTemperature();
       pSonar->setTemperature(temp);
       state = WORKING;
      }
      break;
    }
    case WORKING: {
      if (!pExperiment->interrupted() && !pExperiment->completed()){
        long t = millis();
        long lastTime = pExperiment->getLastTime();
        if (t - lastTime >= dt){
          float dist = pSonar->getDistance();       
          if (dist == NO_OBJ_DETECTED){
            pExperiment->notifyObjLost();
            // Logger.log(String("TRACKTASK | lost object"));
          } else {
            // Logger.log(String("TRACKTASK | update ") + dist);
            pExperiment->updateData(dist);
            long time = pExperiment->getLastTime();
            double pos = pExperiment->getLastPos();
            double vel = pExperiment->getLastVel();
            double acc = pExperiment->getLastAcc();
            pUserConsole->notifyNewData(time, pos, vel, acc);
          }
        } 
      } else {
        state = IDLE;
      }
      break;
    }
  }
};
