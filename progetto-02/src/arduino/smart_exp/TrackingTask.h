#ifndef __TRACKINGTASK__
#define __TRACKINGTASK__

#include "Task.h"
#include "Sonar.h"
#include "Experiment.h"
#include "UserConsole.h"
#include "TempSensor.h"

#define BASE_PERIOD 10

class TrackingTask: public Task {

public:
  TrackingTask(Experiment* pExp, UserConsole* pUserConsole); 
  void init();
  void tick();

private:
  enum { IDLE, WORKING, ERROR } state;
  long time;
  long dt;
  Sonar* pSonar;
  TempSensor* pTemp;
  
  Experiment* pExperiment;
  UserConsole* pUserConsole;
  
};

#endif
