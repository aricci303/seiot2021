#ifndef __EXPERIMENT__
#define __EXPERIMENT__

typedef enum {FREQ_1HZ, FREQ_5HZ, FREQ_25HZ, FREQ_50HZ} TTrackFreq;

class Experiment {

public:
  
  Experiment();
  
  void init();
  void updateData(double newPos);

  long getLastTime();
  float getLastPos();
  float getLastVel();
  float getLastAcc();

  bool started();
  bool completed();
  bool interrupted();
  bool isObjLost();
  
  void setTrackingFreq(TTrackFreq freq); 
  TTrackFreq getTrackingFreq();

  void notifyObjLost();
  void setInterrupted();  
  void setCompleted();  

private:
  enum { NOT_STARTED, ONGOING, INTERRUPTED, COMPLETED } state;

  TTrackFreq trackFreq;
  long startTime;
  long time;
  double pos;
  double vel;
  double acc;
  bool objLost;
};

#endif
