/*
 *  This header file stores symbols that concerns 
 *  the configuration of the system.
 *
 */
#ifndef __CONFIG__
#define __CONFIG__

#define __DEBUG__

#define LED01_PIN 9
#define LED02_PIN 8
#define LED03_PIN 7
#define LED04_PIN 6

#define LED_START 10

#define BUT01_PIN 5
#define BUT02_PIN 4
#define BUT03_PIN 3
#define BUT04_PIN 2

#define POT_PIN A0

#define TMIN 4000


#endif
