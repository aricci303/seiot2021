#include "Arduino.h"
#include "config.h"

#define NLEDS 4

uint8_t ledPin[] = {LED01_PIN, LED02_PIN, LED03_PIN, LED04_PIN};
uint8_t pulseIntensity = 0;
uint8_t pulseDelta = 5;

void initLedBoard(){   
  for (int i = 0; i < NLEDS; i++){
    pinMode(ledPin[i], OUTPUT);     
  }
  pinMode(LED_START, OUTPUT);     
}

void resetLedBoard(){   
  for (int i = 0; i < NLEDS; i++){
    digitalWrite(ledPin[i], LOW);     
  }
}

void testLedBoard(){   
  for (int i = 0; i < NLEDS; i++){
    digitalWrite(ledPin[i], HIGH);     
    delay(500);
    digitalWrite(ledPin[i], LOW);     
  }
  for (int i = 0; i < 10; i++){
    analogWrite(LED_START, i*25);     
    delay(100);
  }
  for (int i = 9; i >= 0; i--){
    analogWrite(LED_START, i*25);     
    delay(100);
  }
}

void resetPulsing() {
  pulseIntensity = 0;
  pulseDelta = 5;
  analogWrite(LED_START, pulseIntensity);   
}

void goOnPulsing(){
  analogWrite(LED_START, pulseIntensity);   
  pulseIntensity = pulseIntensity + pulseDelta;
  if (pulseIntensity == 0 || pulseIntensity == 255) {
    pulseDelta = -pulseDelta ; 
  }     
  delay(15);                               
}

void switchOffLed(uint8_t pos){
  digitalWrite(ledPin[pos], LOW);
}

void switchOnLed(uint8_t pos){
  digitalWrite(ledPin[pos], HIGH);
}
