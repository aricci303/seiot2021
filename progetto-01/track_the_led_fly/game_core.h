#ifndef __GAME_CORE__
#define __GAME_CORE__

#include "Arduino.h"

#define GAME_INTRO 1
#define GAME_WAIT_TO_START 2
#define GAME_INIT 3
#define GAME_LOOP 4
#define GAME_OVER 5

extern uint8_t gameState;

void gameIntro();
void gameWaitToStart();
void gameInit();
void gameLoop();
void gameOver();

/* game test */
void gameTest();

#endif
