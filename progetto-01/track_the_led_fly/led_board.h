/* 
 * Module about the led board. * 
 */
#ifndef __LED_BOARD__
#define __LED_BOARD__

#include "Arduino.h"

void initLedBoard();
void resetLedBoard();
void switchOffLed(uint8_t pos);
void switchOnLed(uint8_t pos);
void resetPulsing();
void goOnPulsing();

/* for testing */ 
void testLedBoard();

#endif
